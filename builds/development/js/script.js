function animate(selector, animLenght) {
    $(document).ready(function () {
        $(selector).animate({opacity: 1}, animLenght);
    });
}

var selectors = [
    '.middle-parent img',
    '.desc',
    '.soon',
    '.phone',
    '.mail',
    '.copyright'
];

var animLenght = 100;

for (elem in selectors) {
    animLenght += 250;
    //$(selectors[elem]).css("opacity", 0);
    setTimeout(animate, animLenght, selectors[elem], animLenght);
}